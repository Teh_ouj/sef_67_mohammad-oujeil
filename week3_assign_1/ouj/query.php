<?php
// Problems: 
// - we have multiple databases, how do we select a particular one for query. 
// - each database has multiple tables, how do we select a particular one for query. 
// - TODO check for names that may have characters which would fuck up the file functions. 

class parser {
  function assertArgumentCountGE($source, $argv, $n) {
    $argc = count($argv);
    if ($argc < $n) throw new Exception($source . ' expected ' . $n . ' or more arguments, however ' . $argc . ' arguments present.');
  }

  function assertArgumentCountEQ($source, $argv, $n) {
    $argc = count($argv);
    if ($argc != $n) throw new Exception($source . ' expected ' . $n . ' arguments, however ' . $argc . ' arguments present.');
  }


  function createDatabase($argv) {
    assertArgumentCountEQ('CREATE DATABASE', $argv, 1);
    $name = array_shift($argv);   
    // TODO check if conflicting name exists. 
    print('"' . $name . '" CREATED' . "\n");
  }

  function createTable($argv) {
    assertArgumentCountGE('CREATE TABLE ', $argv, 1);
    $name = array_shift($argv);   
    if ('COLUMNS' !== array_shift($argv)) throw new Exception('invalid syntax');
    // TODO check if conflicting name exists. 
    $columns = $argv;
    print('"' . $name . '" CREATED' . "\n");
  }


  function deleteDatabase($argv) {
    assertArgumentCountEQ('DELETE DATABASE', $argv, 1);
    $name = array_shift($argv);   
    // TODO check if database exists. 
    print('"' . $name . '" DELETED' . "\n");
  }

  function deleteTable($argv) {
    assertArgumentCountEQ('DELETE TABLE', $argv, 1);
    $name = array_shift($argv);   
    // TODO check if table exists. 
   print('"' . $name . '" DELETED' . "\n");                     
}


function createCommand($argv) {
  assertArgumentCountGE('CREATE', $argv, 1);
  $type  = array_shift($argv);
  switch ($type) {
    case 'DATABASE': createDatabase($argv); break;
    case 'TABLE'   : createTable($argv) ;   break;
  }
}

function addRow($argv) {
  //assertArgumentCountEQ('ADD ROW', $argv, sizeof($columns));
  // TODO check record added has right number of cols, i.e. matches the table
  //print("TODO: add the record to the database.\n");
  print("Record ADDED\n");
}

function getRow($argv) {
  assertArgumentCountEQ('GET ROW', $argv, 1);
  $key = array_shift($argv);
  // TODO: search for record with the proper value in the columns
  print("TODO RETURN RECORD\n");
}


function deleteRow($argv) {
  assertArgumentCountEQ('DELETE ROW', $argv, 1);
  $key = array_shift($argv);
  print("Record DELETED\n");
}


function deleteCommand($argv) {
    assertArgumentCountGE('DELETE', $argv, 1);
    $type  = array_shift($argv);
    switch ($type) {
     case 'DATABASE': deleteDatabase($argv); break;
     case 'TABLE'   : deleteTable($argv);    break;
     case 'ROW'     : deleteRow($argv);      break;
   }  
  }


function query($query) {
  $query  = trim(preg_replace('/^--.*?$/', '', trim($query)));
  if ($query === '') return;

  $argv   = str_getcsv($query);
  if (sizeof($argv) == 0) return;

  $command = array_shift($argv);
  switch ($command) {
    case 'CREATE': return createCommand($argv); break;
    case 'DELETE': return deleteCommand($argv); break;
    case 'ADD'   : return addRow($argv);     break;
    case 'GET'   : return getRow($argv);     break;
    default      : throw new Exception('SYNTAX ERROR');
  }
}


