-- Create a Database
CREATE,DATABASE,"Database Name"

-- Delete a Database
DELETE,DATABASE,"Database Name"

-- Create a table (Number of columns is indefinite)
CREATE,TABLE,"TABLENAME",COLUMNS,"Column1","Column2","Column3"

-- Add a record (The table has a non-null constraint on all columns)
ADD,"10","Bassem","Dghaidi","SEF Instructor"

-- Retrieve a record
GET,"10"

-- Delete a record
DELETE,ROW,"10"
