<?php

require_once('DB.php');
class DBManager {

	private $user;
	private $pass;
	private $thisDir;
	public $key = false;
	const AUTHTABLE = 'Authentication.json';
	const META = 'Meta.json';
	const ROOTDIR = '/home/oj/sefprojects/sef_67_mohammad-oujeil/week
	_assign_1/db';
	

	function createDB($dbName, $user, $pass) {

		$manage = $this->if_DBavail($dbName);
		if($manage < 0) {

			$dbPath = $this->DBfilePath($dbName);
			mkdir($dbPath);
			echo $dbPath;
			$this->createAuth($user, $pass, $dbPath);
			return 0;

		} else {

			echo 'Database already Exists!';
			return $manage;

		}

	}

	private function createAuth($user, $pass, $dbPath) {

		$authTable = $dbPath . '/' . self::AUTHTABLE;
		$table = fopen($authTable, 'w');
		fclose($table);
		$MetaTable = $dbPath . '/' . self::META;
		$table = fopen($MetaTable, 'w');
		fclose($table);
		$toInput[0]['idCount'] = 1;
		$toInput['Authentication']['Id'] = '1';
		$toInput['Authentication']['user'] = 'S*';
		$toInput['Authentication']['pass'] = 'I';
		$jsonData = json_encode($toInput, JSON_PRETTY_PRINT);
		file_put_contents($authTable, $jsonData);
		$toInput = array();
		$toinput[0]['user'] = $user;
		$toInput[0]['pass'] = $pass;
		$jsonData = json_encode($toInput, JSON_PRETTY_PRINT);
		file_put_contents($MetaTable, $jsonData);
		

	}

	private function if_DBavail($dbName) {
		
		$dbExistence = is_dir(self::ROOTDIR . '/' .$dbName);
		if($dbExistence){
			//echo 'nope!!';
			return 0;

		}else {

			return -1;
		}
	}

	function EnterDB($dbName, $user, $pass) {

		$manage = $this->if_DBavail($dbName);
		if ($manage < 0) {

			return -1;

		}else {

			$this->thisDir = $this->DBfilePath($dbName);
			$this->user = $user;
			$this->pass = $pass;  
			return $this->auth();
			
		}

	}

	private function auth() {

		$db = new DB();
		$dbName = $this->thisDir;
		$manage = $db->verification($dbName, $this->user);
		//echo $manage;
		if(is_int($manage)){

			echo 'Authentication failed.';
			$this->key = false;
			return $manage;

		}else {

			if ($this->pass == $manage['pass']){
				echo 'success.';
				$manage = 0;
			}else {

				echo 'Invalid pass.';
				$manage = -1;

			}
			return $manage;

		}

	}

	private function DBfilePath($dbName) {

		$file = self::ROOTDIR . '/' . $dbName;
		return $file;

	}


}