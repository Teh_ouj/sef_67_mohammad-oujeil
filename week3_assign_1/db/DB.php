<?php

class DB {

	const META = "Meta";
	const EXT = '.json';
	private $Dir;
	//creates table with clumns
	function createTable($tableName, $columnNames = array()) {
		//checking if file exists.... returns either a file name or fa
		//lse...
		$file = $this->fileExistence($tableName);
		if($file) {

			echo 'Table already exists.';

		}else {
			//table doesnt exist so create one.
			$file = $tableName . self::EXT;
			$myfile = fopen($file, "w");
			fclose($myfile);
			$file = self::META . self::EXT;
			//array that will be pu in file
			$json = file_get_contents($file);
			$json = json_decode($json);
			$count = $json[0]['idCount']++;
			
			//meta data of each file
			foreach ($columnNames as $columnName) {	

				$toInput[$tableName][$columnName] = $value;

			}
			file_put_contents($file, json_encode($toInput));

		}
	}

	function deleteTable($tableName) {

		$file = $this->fileExistence($tableName);
		if ($file){

			unlink($file);

		}

	}
	//add record
	function addRecord($tableName, $columns =array()) {

		$file = $this->fileExistence($tableName);
		if ($file){

			$contents = file_get_contents($file);
			$toInput = array();
			$count = 0;
			$json = json_decode($contents, true);
			$primary = reset($json[0]);
			$search = $this->findrecord($tableName, $primary, $columns
				[0], 'a');
			if (!is_int($search)){
				echo 'Record Primary key exists!!';
				return -1;

			}
			$temp = $json[0];
			foreach ($temp as $key => $value) {

				$toInput[$key] = $columns[$count++];
			}
			array_push($json, $toInput);
			$jsonData = json_encode($json, JSON_PRETTY_PRINT);
			file_put_contents($file, $jsonData);
		}else {

			echo 'file does not exist';

		}
	}

	private function findrecord($tableName, $columnName, $columnValue,
		 $caller) {

		$file = $this->fileExistence($tableName);
		//echo $file;
		if ($file) {
			echo 'yay!!';
			$contents = file_get_contents($file);
			$toInput = array();
			$count = 0;
			$json = json_decode($contents, true);
			//var_dump($json);
			for ($i = 1; $i < count($json); $i++) {
				if ($json[$i][$columnName] == $columnValue) {
					if ($caller == 'd'){

						echo 'found!!';
						$toReturn['index'] = $i;
						$toReturn['contents'] = $json;
						return $toReturn;
					}else { 

						return $json[$i];

					}

				}	
			}
			return -2;
		}else {

			return -1;

		}

	}

	function getRecord($tableName, $columnName, $columnValue) {
			
		return $this->findrecord($tableName, $columnName, $columnValue
			, 'g');

	}

	function deleteRecord($tableName, $columnName, $columnValue) {

		$content = $this->findrecord($tableName, $columnName, $columnV
			alue, 'd');
		if ($content < 0) {

			echo 'File doesnt exist or record doesnt exist.';

		}else {

			$tablContent = $content['contents'];
			$index = $content['index'];
			array_splice($tablContent, $index, 1);
			$jsonInp = json_encode($tablContent, JSON_PRETTY_PRINT);
			$file = $tableName . self::EXT;
			file_put_contents($file, $jsonInp);
		}

	}

	function fileExistence($ffname) {

		$file =$this->dir . '/' . $ffname . self::EXT;
		if(file_exists($file)) {
			
			return $file;

		}else {
			return false;	
		}

	}

	function verification($dbName, $username) {

		$authTable = 'Authentication';
		$this->dir = $dbName;
		return $this->findrecord($authTable, 'user', $username, 'A');

	}


}