<?php

class customer {

	private $customerInfo;
	private $custBymail = "SELECT * FROM customer where email = '%s'";

	function __construct($db, $email) {

		$result = $db->query(sprintf($this->custBymail, $email));
		$numRows = $result->num_rows;
		if($this->valid($numRows)) {

			$result->data_seek(0);
			$this->customerInfo = $result->fetch_array(MYSQLI_ASSOC);
			$_SESSION['customerInfo'] = $this->customerInfo;

		}else {
			$_SESSION['passError'] = 1;
			$_SESSION['LAST_ACTIVITY'] = time();
			header('location: Login.php');
			exit();
		}
		

	}

	function getInfo() {

		return $this->customerInfo;

	}

	private function valid($numRows) {

		if (empty($numRows)) {
			echo 'empty';
			return false;

		}else {
			return true;
		}

	}


}

?>