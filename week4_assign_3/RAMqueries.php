<?php


class RAMqueries {

	private $movieListQ = 
		"SELECT 
	    DISTINCT F.title, F.film_id
		FROM
	    film AS F
	        JOIN
	    inventory AS inv ON inv.film_id = F.film_id
	        AND inv.store_id = '%s'
	        LEFT JOIN
	    (SELECT 
	        F.title AS FilmTitle,
	            INV.inventory_id AS InventoryId,
	            F.film_id AS FilmId
	    FROM
	        film AS F
	    JOIN inventory AS INV ON INV.film_id = F.film_id
	        AND INV.store_id = '%s'
	    JOIN rental AS RENT ON RENT.return_date IS NULL
	        AND RENT.inventory_id = INV.inventory_id) AS S ON 
	        S.InventoryId = inv.inventory_id
		WHERE
	    S.InventoryId IS NULL";



	private $rentedQuery = 
		"SELECT 
    	f.title, inv.inventory_id, inv.film_id
		FROM
    	rental AS r
        JOIN
    	inventory AS inv ON inv.inventory_id = r.inventory_id
        JOIN
    	film AS f ON f.film_id = inv.film_id
		WHERE
    	r.return_date IS NULL
        AND r.customer_id = %s";

    private $toRentfilmId = 
    	"SELECT 
    	flist.inventory_id
		FROM
    	(SELECT 
        inv.inventory_id
	    FROM
        inventory AS inv
	    WHERE
        inv.store_id = '%s' AND inv.film_id = '%s') AS flist
        LEFT JOIN
	    rental AS r ON r.return_date IS NULL
        AND r.inventory_id = flist.inventory_id
		WHERE
	    r.inventory_id IS NULL";

	private $forRenting = 
		"SELECT 
		i.inventory_id
		FROM
		(SELECT 
		inv.inventory_id
		FROM
		inventory AS inv
		WHERE
		inv.film_id = '%s' AND inv.store_id = '%s') AS i
		LEFT JOIN
		rental AS r ON r.return_date IS NULL
		AND r.inventory_id = i.inventory_id
		WHERE
		r.inventory_id IS NULL";

	private $rent = "INSERT INTO `rental` (`inventory_id`, 
		`customer_id`, `staff_id`) VALUES ('%s', '%s', '%s')";

	private $ifRented = 
		"SELECT 
		inv.film_id, inv.inventory_id, r.customer_id
		FROM
		inventory AS inv
		JOIN
		rental AS r ON r.return_date IS NULL
		AND r.inventory_id = inv.inventory_id
		AND r.customer_id = '%s'
		WHERE
		inv.film_id = '%s'";

	function movieLquery() {
		return $this->movieListQ;
	}

	function rentedMquery() {
		return $this->rentedQuery;
	}

	function toRent() {
		echo 'dude';
		return $this->rent;
	}

	function checkIfrented() {
		return $this->ifRented;
	}

	function needInvid() {
		return $this->forRenting;
	}
}
?>