<?php
//my wrapper class
require_once 'MySQLWrap.php';
session_start();
//this initializes and creats the connection to db
$wrapper = new MySQLWrap();
//message to be displayed later due to possible errors
$message = 'What will it be?';
//if arrival to here was from a redirect and there is already an
//instance of custumerInfo then i create db and wrapper and add
//dustomer...
if(isset($_SESSION['customerInfo'])) {
	//if an instance exists then set wrappers customerInfo to that 
	//instance
	$wrapper->setCustomer();
	//echo 'is set!!';

}else {
	//if not here from a redirect then its a first time
	//auth user and add him to class
	$wrapper->authUser($_POST['Email']);
//if rented is init. then i tried to rent and found that i have
//already rented it!!
}if(isset($_SESSION['Rented'])) {
	$message = 'oops, already rented it!!';
	unset($_SESSION['Rented']);
	//twas supposed to pat the user on the back for being able to rent.
}if(isset($_SESSION['done'])) {
	//fone gets set and is used to alter $message to hold appropriate
	//message
	$message = 'Done!! Check the list to below to confirm.';

}
//first name of user
$name = $wrapper->getFromcust('first_name');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Rent A Movie Wlo!!!</title>
</head>
<body>
<h1>Rent A Movie!!</h1>
<h2>
Hi <?php echo $name;?>
! Welcome to Rent A Movie!!!
</h2>
<h3><?php echo $message; ?></h3>
<?php 
//get movie list is a funct in wrapper and gives me a result 
//of a query that gets list of available movies
$result = $wrapper->getMovielist();
?>
<form action="OrderProcess.php" method="post">
  <label for="movies">Movies to rent:</label>
  	<select name="movies">
		<option value="">-select-</option>
		<?php //as long as there are rows, while continues...
		while ($row = $result->fetch_assoc()){
		?>
    		<option value='<?php echo $row['film_id']; ?>'><?php echo 
    		$row['title']; ?></option>
		<?php    
			}
		?>
	</select>
	<input type="submit" value='Rent'>
</form>
<form action="giveBack.php" method="post">
<?php $result = $wrapper->getRentedlist(); ?>
  Movies rented:<br>
  	<ul>
		<?php
		while ($row = $result->fetch_assoc()){
			?>
    	<li>
    	<input type="checkbox" name="return[]" 
    	value='<?php $row['inventory_id']; ?>'/>
    	<?php echo $row['title']; ?>
    	</li>	
		<?php }	?>
	</ul>
	<input type="submit" name="formSubmit" value="Give back" />
</form>
<a href="logout.php">Logout</a>
</body>
</html>




