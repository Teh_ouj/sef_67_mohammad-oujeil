<?php

require_once 'connectDB.php';
require_once 'customer.php';
require_once 'MovieList.php';
require_once 'RAMqueries.php';
Class MySQLWrap {
	//will retrieve all needed queries
	private $query;
	//will have a row full of info of cust
	private $customerInfo;
	//object that connects the database...
	private $dbConnect;
	//instance of db
	private $db;
	//creates at init a query object a db handler and eventually the
	//db
	function __construct() {
		$this->query = new RAMqueries();
		$this->dbConnect = new connectDB();
		$this->db = $this->dbConnect->connect($this->db);

	} 
	//creates class customer and retreavies info row...
	function authUser($email) {
		$customer = new customer($this->db, $email);
		$this->customerInfo = $customer->getInfo();

		
	}
	//retrieves from xustomer info what is needed
	function getFromcust($field) {
		return $this->customerInfo[$field];

	}
	//gets from instance of queries the query it needs to query it by
	//the DB
	function getMovielist() {
		$movies = $this->query->movieLquery();
		$movies = sprintf($movies,$this->customerInfo['store_id'],
			$this->customerInfo['store_id']);
		return $this->db->query($movies);
	}

	function getRentedlist() {
		$movies = $this->query->rentedMquery();
		$movies = sprintf($movies,$this->customerInfo['customer_id']);
		return $this->db->query($movies);		
	}

	private function rent($filmId) {
		$inv = $this->query->needInvid();
		$inv = sprintf($inv, $filmId, 
			$this->customerInfo['store_id']);
		$result = $this->db->query($inv);
		if($result->num_rows <= 0) {
			$_SESSION['Rented'] = 1;
			header('location:order.php');
			exit();	
		}
		$row = $result->fetch_assoc();
		$invID = $row['inventory_id'];

		$rent = $this->query->toRent();
		$rent = sprintf($rent,$invID, 
			$this->customerInfo['customer_id'], 1);
		$result = $this->db->query($rent);
		if($result) {
			$_SESSION['Done'] = 1;
		}else {
			$_SESSION['Done'] = 0;
		}
		header('location:order.php');
		exit();

	}

	function checkRented($filmId) {

		$ifRented = $this->query->checkIfrented();
		$ifRented = sprintf($ifRented,
			$this->customerInfo['customer_id'],$filmId);
		$result = $this->db->query($ifRented);
		if($result->num_rows > 0) {
			$_SESSION['Rented'] = 1;
			header('location:order.php');
			exit();
		}else {
			$this->rent($filmId);
		}
	}

	function setCustomer() {
		$this->customerInfo = $_SESSION['customerInfo'];
	}
}

?>