#!/usr/bin/php

<?php

$shortopt = "a:e:t:d:s:m:";
//array of commands and their values
$options = getopt($shortopt);
//size of argv array without the filename
$sizeargs = count($argv) -1;
//size of options array
$sizeopt = count($options);

if ($sizeopt == 0){
	echo "no args passed or invalide args!!\n";
}
else if (($sizeopt != $sizeargs) && ($sizeopt != $sizeargs/2 || $sizeopt == 0 || $sizeargs == $sizeopt + 1)){
	echo "incorrect use of arguments!!\n";
}
//if ($sizeargs % 2 == 0 && $sizeargs != 2) {

//	echo "Missing parameters!\n";

//}else if (count($options) == 0) {
	
//	echo "no commands passed or missing parameters!!\n";

//} 
//else {
//	chdir('/home/oj/sefprojects/');
//	$result = dealWithargs($options);
//	var_dump($result);

//}

function dealWithargs($options){

	exec("git log --pretty=format:'%H - %an - %ad - %s - %ae'", $output);

	foreach ($options as $key => $value) {
		//echo $value;
		switch ($key) {

			case 'a':
			$output = searchAuth($value, $output);
			break;

			case 'e':
			$output = searchEmail($value, $output);
			break;

			case 't':
			$output = searchTime($value, $output);
			break;

			case 'd':
			$output = searchDate($value, $output);
			break;

			case 's':
			$output = searchepoch($value, $output);
			break;

			case 'm':
			$output = searchMess($value, $output);
			break;
		}	
	}
	return $output;
}	
	
//var_dump($argv);

function searchMess($val, $output){
	$atMess = "/.*-.*-.*-.*(".$val.").*-.*/()";
	$count = 0;
	$restoret = array();
	foreach ($output as $line) {
		preg_match($atMess, $line, $match);
		if (count($match) > 0) {
			//echo $match[0];
			$restoret[$count++] = $line;
		}
		
	}
	return $restoret;

}

function searchTime($val, $output){
	$atTime = "/.*-.*-.*(".$val.":\d+).*-.*-.*/";
	$restoret = array();
	$count = 0;
	foreach ($output as $line) {
		preg_match($atTime, $line, $match);
		if (count($match) > 0){
			$restoret[$count++] = $line;
		}
	}
	return $restoret;
}
//needs reworking
/*function searchepoch($val, $output){
	$restoret = array();
	$count = 0;
	foreach ($output as $line) {
		preg_match('/'.$val.'/', $line, $match);
		$restoret[$count++] = $line;
	}
	return $restoret;

}*/

function searchEmail($val, $output){

	$atEmail = "/.*-.*-.*-.*-.*(".$val.").*/";
	$restoret = array();
	$count = 0;
	foreach ($output as $line) {
		preg_match($atEmail, $line, $match);
		if (count($match) > 0){
			$restoret[$count++] = $line;
		}
	}
	return $restoret;

}

function searchAuth($val, $output){
	$val = ucfirst($val);
	$count = 0;
	$atAuth = "/.*-.*(".$val.").*-.*-.*-.*/";
	$restoret = array();
	foreach ($output as $line) {
		preg_match($atAuth, $line, $match);
		//echo $line;
		if (count($match) > 0){
			$restoret[$count++] = $line;
		}
	}
	return $restoret;

}
//needs reworking
/*
function searchDate($val, $output){
	$old_date = date('l, F d y h:i:s');
	$restoret = array();
	$count = 0;
	foreach ($output as $line) {
		preg_match('/'.$val.'/', $line, $match);
		$restoret[$count++] = $match;
	}
	return $restoret;

}*/
?>