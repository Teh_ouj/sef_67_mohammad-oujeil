#!/usr/bin/php

<?php

	try{

		$file = "/var/log/apache2/access.log";

		if (!file_exists($file)){

			throw new Exception('File not found.');

		}
		//open file stream
		$myFile = fopen($file, "r");
		//while there are lines in the stream, put the line in variable $line else returns false and stop looping
		while($line = fgets($myFile)){

			//matching all instances of regular expression of an ip address and inserts them in $match
			preg_match("(\d+\.\d+\.\d+\.\d+)", $line, $match);

			$ip = $match[0];

			//matching regular expression to the date from $line
			preg_match("(\d+\/[A-z]+\/\d+)", $line, $match);
			//since only one instance of date per line so fist index is the date
			$date = $match[0];
			//seperates $date into an array where each cell is a string that is seperated by /
			$sep = explode("/", $date);
			$day = $sep[0];
			$month = $sep[1];
			$year = $sep[2];
			//changing the format of $date so funct timestamp takes it as an arg
			$date = $year . "-" . $month . "-" . $day;
			//strtotime takes in an english date format and parses into a unix timestamp 
			$timestamp = strtotime($date);
			//returns the day abbreviated from timestamp
			$day = date('l', $timestamp);
	
			//regular expression matching time from the line
			preg_match("(\:\d+\:\d+\:\d+)", $line, $match);

			$time = $match[0];
			//since time extracted is seperated with : replacing them with - and trimming an extra one from the left
			$time = str_ireplace(":", "-", $time);
			$time = ltrim($time, "-");
			//regex matching of the http respinse message
			preg_match('/\"([A-z]+\s[*\/])(([A-z]*\/)*([A-z-._0-9]+))*\s[A-z]+\/[01.]+\"\s\d+/', $line, $match);

			$httpmes = $match[0];
			//regex matching from the http response message the number of the response
			preg_match('/\d\d\d/', $httpmes, $match);

			$httpnum = $match[0];
			//trim out the response number from http response message to add the spaces and double dash
			$httpmes = rtrim($httpmes, $httpnum);

			printf("%s -- %s, %s %s : %s -- %s-- %s\n", $ip, $day, $month, $year, $time, $httpmes, $httpnum);

		}
		fclose($myFile);

	}catch(Exception $e){
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}

?>