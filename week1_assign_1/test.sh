#!/bin/bash

var1="Hello, World"
echo $var1

if [ -f /home/oj/Desktop/file.csv ] ; then 
	rm /home/oj/Desktop/file.csv
fi

for file in /var/log/*.log;
do 
filesize=$(stat -c%s $file)
filesize=$((filesize/1000))
printf '%s \n %s' ${file:9} ',' $filesize | paste -sd ' ' >> /home/oj/Desktop/file.csv

done