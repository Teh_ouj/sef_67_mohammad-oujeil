function getTodoList() {
	//
	var toDoTitle = document.getElementById("todo-title").value;
	var checkIfspace = /^(\s*\S)/;
	if (!toDoTitle) {
		console.log('empty');
		return;
	}
	var toDoDesc = document.getElementById("todo-details").value;
	if (!toDoDesc) {
		console.log('empty');
		return;
	}
	if(!checkIfspace.test(toDoTitle) || !checkIfspace.test(toDoDesc)) {
		return;
	}
	var currentdate = new Date();
	var weekdays = ['sunday','monday','tuesday','wednesday',
		'thursday','friday','saturday'];
	var months = ['January','february','march','april','may',
		'june','july','august','september','october','november','december'];
	var monthName = months[currentdate.getMonth()];
	var dayName = weekdays[currentdate.	getDay()];
	var datetime = 	"ADDED:" + dayName + ", "
	                +  monthName + " "
	                + currentdate.getDate() + " " 
	                + currentdate.getFullYear() + " "  
	                + currentdate.getHours() + ":"  
	                + (currentdate.getMinutes()<10?'0':'') + currentdate.getMinutes();
	console.log(toDoTitle);
	console.log(toDoDesc);
	console.log(datetime);
	elementCreation(toDoTitle, toDoDesc, datetime);

}

function elementCreation(title, description, dateFormat)
{   
    

    // Get the element from DOM
    var toDoList = document.getElementById("todo-list");
    console.log(toDoList.id)
    // create div instance item...
    var item = document.createElement('DIV');
    // set className of item to appropriate class...
    item.className = "todo-data-holder";
    // create instance of delete button for item instance... 
    var cancelButtcol = document.createElement('DIV');
    cancelButtcol.className = 'todo-cancel-col';
    // innerhtml...
    var buttDel = document.createElement('BUTTON');

    cancelButtcol.appendChild(buttDel);

    
    var inputElements = '<div class="todo-data-col">' +
							'<h2>' + title + '</h2>' +
							'<small>' + dateFormat + '</small>' +
							'<p>' + description + '</p>' +
						'</div>' +
						'<div class="divider"></div>';
    // insert above html tags and design direct in innerHTML method of item..
    item.innerHTML = inputElements;
    item.appendChild(cancelButtcol);
    toDoList.insertBefore(item, toDoList.childNodes[0]);
    localStorage.setItem(item, item) = item;
    console.log(localStorage.item);

    buttDel.onclick = function removeButtonElement() {
        toDoList.removeChild(item);
        localStorage.removeItem(item);
     
    };
    

     
}	