/**
 * [showUser description]
 * @param  {[type]} userInfo [description]
 * @return {[type]}          [description]
 */
var showUser = function(userInfo) {
    // console.log(userInfo);
    var $user = $('<h4>', { id: 'userInfo', class: '', name: userInfo.id });
    var $placeholder = $('#sign-in');
    $user.text(userInfo.email);
    $placeholder.append($user);
}

/**
 * [buttonsClick description]
 * @return {[type]} [description]
 */
var buttonsClick = function() {
    console.log('buttonsClick');
    //button start Session
    $('#start-session').click(function() {
        startSessionAction();
        activateListeners();
    });
    //button stop session
    $('#stop-session').click(function() {
        closeLinks();
        save({ listen: false });
        stopSessionAction();
        location.reload();
    });
}
/**
 * [waitForReply description]
 * @param  {[string]} response [response from deactivation links in session]
 * @return {null}
 */

var hideStart = function() {
    var $startSession = $('#start-session');
    var $startLabel = $('#label-session');
    $startLabel.hide();
    var $sessionName = $('#session-name');
    $startSession.hide();
    $sessionName.hide();
}
var showStop = function() {
    var $stopSession = $('#stop-session');
    $stopSession.show();
}

var hideStop = function() {
    var $sessionStop = $('#stop-session');
    $sessionStop.hide();
}

var showStart = function() {
    var $sessionStart = $('#start-session');
    var $input = $('#session-name');
    var $labelSession = $('#label-session');
    $sessionStart.show();
    $input.show(); 
    $labelSession.show();
}

//shows the sign in button and sets event listener
/**
 * [showSignIn description]
 * @return {[type]} [description]
 */
var showSignIn = function() {
    var $userInfo = $('#userInfo');
    hideStart();
    hideStop();
    $userInfo.hide();
    var $signInButton = $('#sign-in-button');
    var $signInLabel = $('#sign-in-label');
    $signInLabel.hide();
    $signInButton.show();
    $signInButton.click(function() {
        console.log('clicking');
        chrome.identity.getAuthToken({ 'interactive': true }, function(token) {
            console.log("back token=" + token);
        });
    });
}

/**
 * [removeSignIn description]
 * @return {[type]} [description]
 */
var removeSignIn = function() {
    // console.log('removeSignIn');
    $('#sign-in-button').hide();
    $('#sign-in-label').show();
}

var removeResume = function() {
    var resumeButton = $('.resume-session');
    resumeButton.off();
    resumeButton.toggle();
}

var resume = function() {
    $('.resume-session').click(function(event) {
        var that = this;
        chrome.tabs.query({currentWindow: true, url: ['http://*/*', 'https://*/*']}, function (tabs) {
            if(tabs.length > 0) {
                alert('please exit all tabs first. whos clicked? ' + that.id);
            }else {
                resumeSession(that.id);
            }
        });
    });
}

var newTabLinks = function() {
    $('.my-link').click(function() {
        var url = this.id;
        // console.log(url);
        chrome.tabs.create({url: url});
    });
}

var removeCache = function() {
    chrome.storage.local.clear();
    window.reload;
}

var fillData = function(sessions) {
    if(('user' in sessions)) {
        removeCache();
        return;
    }
    console.log(sessions);
    var $data = $('#session-data-holder');
    var $genericSession = $('.session-wrapper:nth-child(1)');
    for(key in sessions) {
        // console.log(sessions[key]);
        var thisSession = sessions[key];
        var $session = $genericSession.clone();
        $session.toggle();
        $session.attr('id', thisSession.id);
        // console.log(thisSession);
        var $name = $($session).find('h3.session-title:nth-child(1)');
        $name.text(thisSession.name);

        var $createdAt = $($session).find('small.created-at:nth-child(1)');
        $createdAt.text('Created at ' + thisSession.created_at);
        var linksLength = thisSession.links.length;
        var $linkHolder = $($session).find('table.link-holder:nth-child(1)');
        if(linksLength == 0) {
            var $emptyRow = $('<tr></tr>');
            var $emptyData = $('<td></td>');
            $emptyData.text('No links..');
            $emptyRow.append($emptyData);
            $linkHolder.append($emptyRow);
        } else {
            for(var i = 0; i < linksLength; i++) {
                var $linksList = $('<tr></tr>');
                var link = thisSession.links[i];
                var $listItemCheckBox = $('<td></td>');
                var $checkbox = $('<input>', {type: 'checkbox', class: 'input' + 
                    thisSession.id, value: link.link, id: ''});

                $listItemCheckBox.append($checkbox);
                $linksList.append($listItemCheckBox);

                var $listItemLink = $('<td></td>');
                var $linkA = $('<a>', {class: 'my-link', id:link.link}).text(link.link);

                $listItemLink.append($linkA);
                $linksList.append($listItemLink);

                var $listItemRemoveIcon = $('<td></td>');
                var $removeIcon = $('<input>', {type: 'button', class:'center-block'});
                $removeIcon.attr('aria-label', 'Close');
                var $span = $('<span>', {class:'center-text'});
                $span.attr('aria-hidden', 'true');
                $removeIcon.attr('value', 'X');

                // $removeIcon.append($span);
                $listItemRemoveIcon.append($removeIcon);
                $linksList.append($listItemRemoveIcon)

                $linkHolder.append($linksList);
            }
        }
        var resumeButton = $($session).find('a.resume-session:nth-child(1)');
        resumeButton.attr('id', thisSession.id);
        // console.log($session);
        $data.prepend($session);
        // return;
    }
    resume();
    newTabLinks();
    // removeLink();
}