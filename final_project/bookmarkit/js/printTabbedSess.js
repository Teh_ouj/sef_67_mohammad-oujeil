/**
 * [method description]
 * @type {Object}
 */
var method = {
    post: 'post',
    get: 'get',
    put: 'put'
};
/**
 * [contentType description]
 * @type {Object}
 */
var contentType = {
    urlEncoded: 'application/x-www-form-urlencoded'
}
//urls for API to database...
/**
 * [url description]
 * @type {Object}
 */
var url = {
    createUser: 'http://localhost/tabngo/public/api/v1/user',
    createSession: 'http://localhost/tabngo/public/api/v1/session',
    resumeSession: 'http://localhost/tabngo/public/api/v1/session/',
    getSessions: 'http://localhost/tabngo/public/api/v1/session',
    addLink: 'http://localhost/tabngo/public/api/v1/link',
    updateLinks: 'http://localhost/tabngo/public/api/v1/link/',
    closeLink: 'http://localhost/tabngo/public/api/v1/link/close',
    getData: 'http://localhost/tabngo/public/api/v1/getData/'
};
//details for getting active tabs...
/**
 * [queryInfo description]
 * @type {Object}
 */
var queryInfo = {
    //active: true,
    currentWindow: true
};
//to stop from opening a tab with sign in chrome prompt
/**
 * [interactive description]
 * @type {Object}
 */
var interactive = {
    'interactive': false
}

var notNewTabs = {
    counter:0
};

var openChosenTabs = function(id) {
    var counter = notNewTabs.counter;
    var $checkedLinks = $('.input' + id + ':checked');
    $checkedLinks.each(function() {
        chrome.tabs.create({url: this.value});
        counter++;
    });
    chrome.storage.local.set({sessionResume: true});
    // console.log(notNewTabs.dontCheck);
    
    sendToAPI(null, method.get, url.resumeSession + id, contentType.urlEncoded, saveSession);
}

var resumeSession = function(id) {
    openChosenTabs(id);
}

var activateListeners = function() {
    listeners.onMessageReceived();
    listeners.onNewTabNav();
    // listeners.onTabExit();
    listeners.onCommitted();
    // listeners.onUpdatedTabs();
    listeners.onNewTab();
    // listeners.onDomContentLoaded()
    // listeners.onBeforeRequest();
    // listeners.onCompleted();
    // listeners.onBeforeRedirect();
}

var filter = {
    urls: ['*://*/*']
};

var sendMessage = function(tab) {
    this.tab = tab;
    var that = this;
    chrome.tabs.onUpdated.addListener(function onUpdate(tabId, changeInfo, tab) {
        console.log('link/form_submit');
        // console.log(that.tab.url);
        if(tabId == that.tab.id) {
            chrome.tabs.sendMessage(tab.id, {query: 'from?'}, function(response) {
                // console.log(tab.url);
                if('papa' in response) {
                    chrome.tabs.onUpdated.removeListener(onUpdate);
                    link = new Link(tab.title, tab.url, response.papa);
                    console.log(response.papa);
                    link.saveToDB();
                } else {
                    console.log('something went wrong!!');
                }
            });
        }
    });
}
var checkIfForwardBack = function(transitionQualifiers) {
    length = transitionQualifiers.length;
    for(var i = 0; i < length; i++) {
        tq = transitionQualifiers[i];
        if(tq == "forward_back"){
            return false;
        }
    }
    return true;
}
//list of chrome listeners
/**
 * [listeners description]
 * @type {Object}
 */
var listeners = {

    onBeforeRedirect: function() {
        chrome.webRequest.onBeforeRedirect.addListener(function (details) {
            console.log(details);
        }, filter);
    },

    onCompleted: function() {
        chrome.webRequest.onCompleted.addListener(function (details) {
            if(details.type == 'main_frame'){
                chrome.tabs.get(details.tabId, function(tab) {
                    if(tab.url != tab.title) {
                        console.log(details);
                        console.log(tab);
                    }
                    // console.log(tab);
                });
            }
        }, filter);
    },

    onCommitted: function(senderTab) {
        chrome.webNavigation.onCommitted.addListener(function(details) {
            console.log(details);
            if(details.frameId == 0) {
                
                if(details.transitionType == 'typed') {
                    chrome.tabs.get(details.tabId, function(tab) {
                        if(tab.title != 'New Tab'){
                            link = new Link(tab.title, tab.url, null);
                            link.saveToDB();
                            console.log('typed');
                            // console.log(tab);
                        }
                    });
                }
                if(details.transitionType == 'generated') {
                    if(checkIfForwardBack(details.transitionQualifiers)){
                        chrome.tabs.get(details.tabId, function(tab) {
                            link = new Link(tab.title, tab.url, null);
                            // link.saveToDB();
                            // console.log('generated');
                            console.log(link);
                        });
                    }
                }
                if(details.transitionType == 'link') {
                    console.log(notNewTabs);
                    if(checkIfForwardBack(details.transitionQualifiers)){
                        chrome.tabs.get(details.tabId, function(tab) {
                            if(!('openerTabId' in tab)){
                                console.log('link');
                                sendMessage(tab);
                            }
                        });
                    }
                }
                if(details.transitionType == 'form_submit') {
                    console.log('form_submit');
                    chrome.tabs.get(details.tabId, function(tab) {
                        sendMessage(tab);
                    });
                }
            }
        });
    },

    onTabExit: function() {
        chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
            // console.log(removeInfo);
            chrome.storage.local.get('sessionId', function(data) {
                closedLink(tabId, data.sessionId);
            });
        });
    },

    onNewTab: function() {
        chrome.tabs.onCreated.addListener(function (tab) {
            if(tab.title != 'New Tab' && !('openerTabId' in tab)) {
                console.log('new tab');
                listeners.onUpdatedTabs(tab);
            }
        });
    },

    onNewTabNav: function() {
        chrome.webNavigation.onCreatedNavigationTarget.addListener(function(details) {
            // console.log(details);
            chrome.tabs.get(details.tabId, function(tab) {
                this.tab = tab;
                var that = this;
                console.log(tab)
                chrome.tabs.get(tab.openerTabId, function(tab) {
                    link = new Link(that.tab.title, that.tab.url, tab.url);
                    link.saveToDB();
                    // console.log(link);
                });
            });
        });
    },
    //listens to tab is updated
    onUpdatedTabs: function(tab) {
        this.tab = tab;
        var that = this;
        console.log('onupdated');
        chrome.tabs.onUpdated.addListener(function onUpdate(tabId, changeInfo, tab) {
            if(tabId == that.tab.id && tab.url != tab.title){
                chrome.tabs.onUpdated.removeListener(onUpdate);
                this.tab = tab
                that = this; 
                chrome.tabs.get(tab.openerTabId, function(tab) {
                    var link = new Link(that.tab.title, that.tab.url, tab.url);
                    link.saveToDB();
                    console.log(link);
                })
            }
        });
    },
    //listens to when a sign in or sign out has happened and executes a function...
    onSignInChange: function() {
        chrome.identity.onSignInChanged.addListener(function() {
            console.log('onSignInChanged: something happened!!!\ncalling to check user!!');
            userValidation.userCache();
            //location.reload();
        });
    },
    //when something changes in cache...
    onChangedStorage: function() {
        chrome.storage.onChanged.addListener(function(changes, areaName) {
        });
    },
    //fires when this page is loaded
    onLoad: function() {
        $('#restore-tab').click(restore);
            window.onload = function() {
            userValidation.userCache();
            checkForLiveSession();
            // elasticInputs();
        };
    },
    onTabReplaced: function() {
        chrome.tabs.onReplaced.addListener(function(addedTabId, removedTabId) {
            console.log('added: ' + addedTabId + '\nremoved: ' + removedTabId);
        });
    },
    onActivated: function() {
        chrome.tabs.getCurrent(function(tab) {
            chrome.tabs.onActivated.addListener(function(activeInfo) {
                if(tab.id == activeInfo.tabId){
                    // console.log('cant drage me into this!!');
                } else {
                    chrome.tabs.executeScript(activeInfo.tabId, {file: './js/notes.js', runAt: 'document_start'});
                }
            });
        });
    },
    onMessageReceived: function() {
        chrome.runtime.onMessage.addListener(
          function(request, sender, sendResponse) {
           if('urlChange' in request) {
            chrome.tabs.get(sender.tab.id, function(tab) {
            });
                console.log('onMessageReceived');
                var link = new Link('no title', request.urlChange, request.prevURL);
                link.saveToDB();
            }
        });
    }
}

var getData = function() {
    chrome.storage.local.get('user', function(data) {
        sendToAPI(null, method.get, url.getData + data.user.id, null, fillData);
    });
        // getFromCache(['sessions'], 1, sessions);
}

//function that saves data to cache
/**
 * [save description]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
var save = function(data) {
    chrome.storage.local.set(data, function() {
        // console.log('saved! in cache');
    });
}

/**
 * [getFromCache description]
 * @param  {[type]}   keys       [description]
 * @param  {[type]}   keysLength [description]
 * @param  {Function} callback   [description]
 * @return {[type]}              [description]
 */
var getFromCache = function(keys, keysLength, callback) {
    chrome.storage.local.get(keys, function(data) {
        var arr = [];

        if (data == null) {
            console.log('dude?');
        } else {
            Object.getOwnPropertyNames(data).forEach(
                function(val, idx, array) {
                    arr[val] = data[val];
                }
            );
        }
        callback(arr);
    });
}


//checks in the cache if a session is already on...
/**
 * [checkForLiveSession description]
 * @return {[type]} [description]
 */
var checkForLiveSession = function() {

    chrome.storage.local.get(['listen'], function(data) {
        // console.log('checkForLiveSession');
        if (data.listen == true) {
            console.log('listening');
            // startSessionAction();
            activateListeners();
            hideStart();
            removeResume();
            showStop();
            console.log('yesListening');
            buttonsClick();
        } else {
            console.log('not listening');
            stopSessionAction();
        }
    });
}

//shows stop session button
/**
 * [startSessionAction description]
 * @return {[type]} [description]
 */
var startSessionAction = function() {
    // console.log('startSessionAction');
    chrome.storage.local.get(['user'], function(data) {
        if(jQuery.isEmptyObject(data)) {
            console.log('please sign in first!!!');
        } else {
            createSession();
        }
    });
}

//show start session button
/**
 * [stopSessionAction description]
 * @return {[type]} [description]
 */
var stopSessionAction = function() {
    console.log('stopSessionAction');
    hideStop();
    showStart();
    buttonsClick();
}

var closeLinks = function() {
    chrome.tabs.query({currentWindow: true, url: ['http://*/*', 'https://*/*']},
        function(tabs) {
            var toClose = [];
            for(var i = 0; i < tabs.length; i++) {
                toClose[i] = tabs[i].id;
            }
            chrome.tabs.remove(toClose);
        }
    );
}

var restore = function() {
    chrome.storage.local.get(['links', 'sessionId', 'sessionName', 'user', 'sessions'], function(data) {
        // console.log(data.links);
        console.log(data);
    });
}

var checkCache = function(links) {
    // console.log(links);
    var toSave = {links: links};
    save(toSave);
}

//an object to take a single link details and inserted in the links object...
/**
 * [saveLinkDetails description]
 * @param  {[type]} tabId    [description]
 * @param  {[type]} tabTitle [description]
 * @param  {[type]} tabLink  [description]
 * @param  {[type]} parentId [description]
 * @return {[type]}          [description]
 */
function Link(tabTitle, tabLink, parentLink) {
    // console.log('saved a link!!');
    // console.log(tabId + tabTitle + tabLink);
    this.userId = $('#userInfo').attr('name');
    // console.log(parentId);
    this.parentLink = encodeURIComponent(parentLink);
    this.title = tabTitle;
    this.url = encodeURIComponent(tabLink);
    var that=this;
    this.saveToDB = function() {
        chrome.storage.local.get(['sessionId'], function(data) {
            console.log('saveToDB');
            var toSend = 'session_id=' + data.sessionId + '&user_id=' + that.userId +
                '&parent_link=' + that.parentLink + '&title=' + that.title +
                that.tabId + '&link=' + that.url;
            sendToAPI(toSend, method.post, url.addLink, contentType.urlEncoded, checkCache);
            // console.log(toSend);        
        });
    }
    this.checkExistence = function() {
        chrome.storage.local.get('links', function(data) {

            if(jQuery.isEmptyObject(data)) {
                that.saveToDB();
                return;
            }
            for (var i = 0; i < data.links.length; i++) {
                if(data.links[i].tab_id == that.tabId && data.links[i].title != that.title) {
                    console.log('saving ' + that.url);
                    that.saveToDB();
                    return;
                }
            }
        });
    }  
}; 
var closedLink = function(tabId, sessionId) {
    console.log('hello');
    var toSend = 'tab_id=' + tabId + '&session_id=' + sessionId;
    sendToAPI(toSend, method.post, url.closeLink, contentType.urlEncoded, checkCache);
}
/**
 * [sendToAPI send data to the DB]
 * @param  {array}   data     [assoc array of data needed to be read by api]
 * @param  {string}   method   [xmlhttp request method]
 * @param  {string}   url      [api url]
 * @param  {Function} callback [what function should handle the data]
 * @return {[type]}            [no return]
 */
var sendToAPI = function(data, method, url, contentType, callback) {
    var xhr = new XMLHttpRequest();
    // console.log('to send to DB: ' + data);
    // console.log(url);
    xhr.open(method, url, true);
    if(contentType != null){
        xhr.setRequestHeader("Content-type", contentType)
    }
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {

            if (callback == null) {
                console.log((xhr.response));
            } else {
                callback(JSON.parse(xhr.response));//console.log(xhr.response);
            }
        } else if (xhr.status === 422) {
            alert(xhr.response);
        }
    };
    xhr.send(data);
}

//calls getfromcache to get user id and sends callback 
//to createSession that actually sends to api to create session
/**
 * [createSession description]
 * @return {[type]} [description]
 */
var createSession = function() {
    var sessionName = $('#session-name').val();
    var userID = $('#userInfo').attr('name');
    var toSend = 'name=' + sessionName + '&user_id=' + userID;
    sendToAPI(toSend, method.post, url.createSession, contentType.urlEncoded, saveSession);
}

/**
 * [saveSession description]
 * @param  {[type]} session [description]
 * @return {[type]}         [description]
 */
var saveSession = function(session) {
    hideStart();
    removeResume();
    showStop();
    activateListeners();
    data = {
        listen: true,
        sessionName: session.name,
        sessionId: session.id
    }
    chrome.storage.local.remove('links', function() {
        console.log('removed old session links');
    })
    // console.log(data);
    save(data);
    chrome.storage.local.get(['sessionResume'], function(data) {
        if(('sessionResume' in data)) {
            chrome.storage.local.remove('sessionResume');
        } else {
            addLinksToSession();
        }
    });
    console.log(session);
}
/**
 * [addLinksToSession description]
 */
var addLinksToSession = function() {
    // console.log('adding open links');
    chrome.tabs.query(queryInfo, function(tabs) {
        //data represent key value array that will be cached!!
        var filteredLinks = tabLinks.filterURLs(tabs);
        for (var i = 0; i < filteredLinks.length; i++) {
            var index = filteredLinks[i];
            link = new Link(tabs[index].title, tabs[index].url, null);
            // console.log(link);
            link.saveToDB();
        }
    });
}

listeners.onSignInChange();
listeners.onLoad();
//every action done on links is grouped in here...
/**
 * [tabLinks description]
 * @type {Object}
 */
var tabLinks = {
    filterURLs: function(tabs) {
        // console.log('filterURLS');
        
        var link = [];
        for (var i = 0; i < tabs.length; i++) {
            if (tabLinks.filterURL(tabs[i].url)) {
                link.push(i);
            }
        }
        return link;
    },
    filterURL: function(url) {
        var httpExp = new RegExp("^htt(p|ps):\/\/");
        if (httpExp.test(url)) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 *holds everything to do with validation user then caches
 */
var userValidation = {
    //checks if cached user is valid or if there is anyone logged in...
    /**
     * [checkUser description]
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    checkUser: function(data) {
        //response is where user info was cached...
        // console.log(data);
        chrome.identity.getProfileUserInfo(function(userInfo) {
            console.log(userInfo);
            //if userInfo is completely empty then show sign in and clear cache...
            if (jQuery.isEmptyObject(userInfo) || userInfo.id === '') {
                showSignIn();
                chrome.storage.local.clear(function() {
                    console.log('cleared');
                });
                return;
            }
            if (jQuery.isEmptyObject(data) || data.user.email != userInfo.email) {
                userValidation.getTabngoId(userInfo);
            } else {
                // console.log('checkUser: ' + data.response.User.email + ' is already cached and signed in!!');
                showUser(data.user);
                getData();
                // console.log(data);
            }
            removeSignIn();
        });
    },

    // get users or save users to/from cache...
    /**
     * [userCache description]
     * @return {[type]} [description]
     */
    userCache: function() {
        // console.log('userCache');
        getFromCache(['user'], 1, userValidation.checkUser);
    },
    /**
     * [cacheUser description]
     * @param  {[type]} response [description]
     * @return {[type]}          [description]
     */
    cacheUser: function(response) {
        var data = {
            user: response,
        };
        console.log(response);
        // stopSessionAction();
        showUser(response);
        save(data);
    },

    /**
     * [getTabngoId description]
     * @param  {[type]} userInfo [description]
     * @return {[type]}          [description]
     */
    getTabngoId: function(userInfo) {
        // console.log('getTabngoId');
        var toSend = 'email_id=' + userInfo.id + '&email=' + userInfo.email;
        sendToAPI(toSend, method.post, url.createUser, contentType.urlEncoded, userValidation.cacheUser);

    }
};
