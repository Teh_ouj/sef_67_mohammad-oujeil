// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */

chrome.browserAction.onClicked.addListener(function (callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  

  var createProperties = {
    url:chrome.extension.getURL("../popup.html")
  };

  chrome.tabs.create(createProperties);

  
}); 