<?php
namespace App\Http\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Jsonable;
use App\Session;
use App\Link;

trait printSessionLinks {

    public function sessions($id) {
        // Get all the brands from the Brands Table.
        $sessions = Session::where('user_id', $id)->orderBy('id', 'desc')->get();
        if(count($sessions) < 1) {
        	return 'no sessions!!';
        }
        $data = [];
        foreach($sessions as $session) {
            $datum = [];
        	$datum['session'] = $session;
            $links = $this->getLinks($session->id);
            $datum['links'] = $links;
        	$data[] = $datum;      
        }
        if(empty($data)) {
        	$data = 'no sessions';
        } else {
            $this->paginateLinks($data);
        }
        // dd($data);
        // return $data;
    }

    public function paginateLinks($data){
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $sessData = collect($data);
        $perPage = 5;
        $currentPageSearchResults = $sessData->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $entries = new LengthAwarePaginator($currentPageSearchResults, count($sessData), $perPage);
        return response()->json($entries);
        // print_r($entries);
    }

    public function getLinks($id) {
    	$links = Link::where('session_id', $id)->get();
        if(count($links) < 1)
            return 'no links!';
    	return $links;
    }
}