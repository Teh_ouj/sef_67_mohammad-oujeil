<?php
 
namespace App\Http\Controllers;
 
use App\Link;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
 
class LinkController extends Controller{
 
	public function createLink(Request $request) {
        $newLink = new Link;
        $newLink->session_id = $request->session_id;
        $newLink->user_id = $request->user_id;
        $newLink->title = $request->title;
        $newLink->link = urldecode($request->link);
        // dd($newLink);
        $parent_link = urldecode($request->parent_link);
        if($parent_link != 'null'){
            // dd($newLink);
            $link = Link::where('session_id', $request->session_id)->where('link', $parent_link)->orderBy('id', 'desc')->get()->first();
            if($link != null){
                $newLink->parent_id = $link->id;
            }
            // return;
        }
        // dd($newLink);
        $newLink->save();
        $links = Link::where('session_id', $newLink->session_id)->get();
        $data = [];
        foreach($links as $link) {
            $data[] = $link;
        } 
        return response()->json($data);
	}
}
?>