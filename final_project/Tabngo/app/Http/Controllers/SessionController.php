<?php
 
namespace App\Http\Controllers;
 
use App\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
class SessionController extends Controller{
 
	public function createSession(Request $request){
        $this->validate($request, [
        'name' => 'required|max:150'
        ]);
        $session = new Session;
        $session->name = $request->name;
        $session->user_id = $request->user_id;
        $session->save();
    	return response()->json($session); 
	}

    public function resumeSession($id) {
        $session = Session::find($id);
        return $session;
    }

	public function search(){
        //echo 'dude';
    	$sessions  = sessions::all();
 
    	return response()->json($sessions);
	}
}
?>