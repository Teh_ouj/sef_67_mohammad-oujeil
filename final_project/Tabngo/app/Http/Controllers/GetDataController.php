<?php
 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use App\User;
use App\Session;
use App\Link;
 
class GetDataController extends Controller{
 
	public function showData($id) {
        $data = array();
        $sessions = User::find($id);
        if($sessions == null) {
            $data['user'] = 'user is not registered!';
            return response()->json($data);
        }
        $sessions = $sessions->sessions;
        foreach($sessions as $session) {
            $links = Session::find($session->id)->links()->where('deleted', false)->orderBy('id', 'desc')->get();
            $data[$session->name] = $session;
            $data[$session->name]->links = $links;
        }
        return response()->json($data);
	}
}
?>