<?php 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Link extends Model
{ 
	public $incrementing = true;
 	protected $fillable = ['session_id', 'user_id', 'link_id', 'title', 'link'];	 
}
?>