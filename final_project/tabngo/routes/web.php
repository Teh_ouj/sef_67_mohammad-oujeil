<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->post('/api/v1/user','UserController@createUser');
$app->get('/api/v1/session','SessionController@index');
$app->post('/api/v1/session','SessionController@createSession');
$app->post('/api/v1/link','LinkController@createLink');
$app->get('/api/v1/getData/{id}','GetDataController@showData');
$app->get('/api/v1/session/{id}', 'SessionController@resumeSession');