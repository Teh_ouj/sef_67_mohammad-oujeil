<?php
 
namespace App\Http\Controllers;
 
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\printSessionLinks;
 
class UserController extends Controller{
    use printSessionLinks;

	public function createUser(Request $request){
        $this->validate($request, [
        'email_id' => 'required',
        'email' => 'required'
        ]);
        $user = User::where('email_id', $request->email_id);
        $userCount = $user->count();
        if($userCount > 0) {
            $data['User'] = $user->first();
            // $data['Sessions'] = $this->sessions($user->first()->id);
            // return response()->json($data);
            return response()->json($user->first()); 
        }
        // var_dump($user);
        $user = User::create($request->all());
        $data['User'] = $user;
        // $data['sessions'] = $this->sessions($user->first()->id);
    	return response()->json($user);
	}
}
?>