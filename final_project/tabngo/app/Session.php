<?php 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Session extends Model
{ 
 	protected $fillable = ['user_id', 'name'];	
 	
 	public function links() {
 		return $this->hasMany('App\Link', 'session_id'); 
 	} 
}
?>