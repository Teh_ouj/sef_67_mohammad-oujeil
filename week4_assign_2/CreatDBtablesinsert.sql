#create database...
CREATE database patient_claims;
#...
USE patient_claims;
creating vlaim table!!
CREATE TABLE Claim (
    claim_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patient_name VARCHAR(30) NOT NULL
);
#creating defendants table
CREATE TABLE Defendants (
    claim_id int not null, 
    defendant_name VARCHAR(30) NOT NULL,
    FOREIGN KEY (claim_id)
        REFERENCES Claim (claim_id)
        ON DELETE CASCADE
);
#creating claimstatuscodes
CREATE TABLE ClaimStatusCodes (
    claim_status varchar(2) NOT NULL PRIMARY KEY,
    claim_status_desc VARCHAR(30) NOT NULL UNIQUE,
    claim_seq INT NOT NULL UNIQUE
);
#creating legalEvents
CREATE TABLE LegalEvents (
    claim_id INT NOT NULL,
    defendant_name VARCHAR(30) NOT NULL,
    claim_status VARCHAR(2) NOT NULL,
    change_date DATE NOT NULL,
    FOREIGN KEY (claim_id)
        REFERENCES Claim (claim_id)
        ON DELETE CASCADE,
    FOREIGN KEY (claim_status)
        REFERENCES ClaimStatusCodes (claim_status)
);

insert into Claim values (null, 'Bassem Dghaidi');
insert into Claim value (null, 'Omar Breidi');
insert into Claim values (null, 'Marwan Sawwan');

INSERT INTO ClaimStatusCodes VALUES('AP', 'Awaiting review panel', 1);
INSERT INTO ClaimStatusCodes VALUES('OR', 'Panel opinion rendered', 2);
INSERT INTO ClaimStatusCodes VALUES('SF', 'Suit filed', 3);
INSERT INTO ClaimStatusCodes VALUES('CL', 'Closed', 4);

insert into Defendants values (1, 'Jean Skaff');
insert into Defendants value (1, 'Elie Meouchi');
insert into Defendants values (1, 'Radwan Sameh');
insert into Defendants values (2, 'Joseph Eid');
insert into Defendants values (2, 'Paul Syoufi');
insert into Defendants values (2, 'Radwan Sameh');
insert into Defendants values (3, 'Issam Awwad');


INSERT INTO LegalEvents VALUES(1, 'Jean Skaff', 'AP', '2016-01-01');
INSERT INTO LegalEvents VALUES(1, 'Jean Skaff',  'OR', '2016-02-02');
INSERT INTO LegalEvents VALUES(1, 'Jean Skaff',  'SF', '2016-03-01');
INSERT INTO LegalEvents VALUES(1, 'Jean Skaff', 'CL', '2016-04-01');
INSERT INTO LegalEvents VALUES(1, 'Radwan Sameh', 'AP', '2016-01-01');
INSERT INTO LegalEvents VALUES(1, 'Radwan Sameh', 'OR', '2016-02-02');
INSERT INTO LegalEvents VALUES(1, 'Radwan Sameh', 'SF', '2016-03-01');
INSERT INTO LegalEvents VALUES(1, 'Elie Meouchi', 'AP', '2016-01-01');
INSERT INTO LegalEvents VALUES(1, 'Elie Meouchi', 'OR', '2016-02-02');
INSERT INTO LegalEvents VALUES(2, 'Radwan Sameh', 'AP', '2016-01-01');
INSERT INTO LegalEvents VALUES(2, 'Radwan Sameh', 'OR', '2016-02-01');
INSERT INTO LegalEvents VALUES(2, 'Paul Syoufi', 'AP', '2016-01-01');
INSERT INTO LegalEvents VALUES(3, 'Issam Awwad', 'AP', '2016-01-01');
