SELECT 
    C.claim_id, C.patient_name, CSC.claim_status
FROM
    (SELECT 
        LE.claim_id, MIN(LE.current_status) AS min_stat
    FROM
        (SELECT 
        LV.claim_id, LV.defendant_name, COUNT(*) AS current_status
    FROM
        LegalEvents AS LV
    GROUP BY LV.defendant_name , LV.claim_id) AS LE
    GROUP BY LE.claim_id) AS current_seq
        JOIN
    Claim AS C ON C.claim_id = current_seq.claim_id
        JOIN
    ClaimStatusCodes AS CSC ON CSC.claim_seq = current_seq.min_stat