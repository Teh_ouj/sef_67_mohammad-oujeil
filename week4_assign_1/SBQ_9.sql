#Get the top 3 customers who rented the 
#highest number of movies within a given year.
SELECT 
    CONCAT(C.first_name, C.last_name) as Customer, count(*) as '# of rentals', 
    R.rental_date as 'Date'
    
FROM
    rental AS R
        JOIN
    customer AS C ON C.customer_id = R.customer_id and YEAR(R.rental_date) = 2005
GROUP BY R.customer_id
