#What are the top 3 languages for movies released in 2006?
SELECT L.name, count(*)
FROM language as L
JOIN film as F on F.language_id = L.language_id and F.release_year = 2006
group by L.name
order by count(*) desc
limit 3
