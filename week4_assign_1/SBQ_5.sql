#Return the first and last names of actors who played in a film
#involving a “Crocodile” and a “Shark”, alongwith the release year of the movie,
#sorted by the actors’ last names
select concat(A.first_name, ' ', A.last_name) as 'Actor', F.release_year
from actor as A
join film_actor as FA on FA.actor_id = A.actor_id
join film as F on F.description like '%Crocodile%' and F.description like '%Shark%' and F.film_id = FA.film_id
order by A.last_name asc