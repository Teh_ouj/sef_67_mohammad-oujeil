#Find the names (first and last) of all the actors and costumers whose first name is the same as 
#the firstname of the actor with ID 8. Do not return the actor with ID 8 himself. 
#Note that you cannot use the name ofthe actor with ID 8 as a constant (only the ID). 
#There is more than one way to solve this question, but youneed to provide only one solution.
SELECT 
    C.first_name, C.last_name
FROM
    customer AS C
WHERE
    C.first_name = (SELECT 
            first_name
        FROM
            actor
        WHERE
            actor_id = 8) 
UNION SELECT 
    A.first_name, A.last_name
FROM
    actor AS A
WHERE
    Ad.first_name = (SELECT 
            first_name
        FROM
            actor
        WHERE
            actor_id = 8)
        AND A.actor_id != 8


