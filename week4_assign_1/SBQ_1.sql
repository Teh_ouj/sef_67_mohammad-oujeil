SELECT concat(A.first_name,' ',A.last_name) as 'Actor name', count(*) as 'Movies'
FROM actor as A 
JOIN film_actor as FA  ON FA.actor_id = A.actor_id
group by A.actor_id
