#What are the top 3 countries from which customers are originating?
select C.country, count(*)
from country as C
join city as CT on CT.country_id = C.country_id
join address as A on A.city_id = CT.city_id
join customer as CST on CST.address_id = A.address_id
group by C.country
order by count(*) desc;