#Get the total and average values of rentals per month per year per store
SELECT 
    SUM(P.amount) AS total, AVG(P.amount) AS average
FROM
    payment AS P
        JOIN
    staff AS S ON P.staff_id = S.staff_id
GROUP BY S.store_id , MONTH(P.payment_date) , YEAR(P.payment_date)