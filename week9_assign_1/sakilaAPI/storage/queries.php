<?php

class queries {
	private $where = 'where';
	private $orderby = 'orderby';
	private $limit = 'limit';
	private $set = 'set';
	//model name;
	private $table;
	private $selectAll;
	private $delete;
	private $insert;
	private $update;

	//construct takes the model name to contruct queries
	function __construct($tableName) {
		$this->table = $tableName;
		$this->selectAll = "select * from $this->table";
		$this->delete = "delete from $this->table";
		$this->insert = "insert into $this->table (";
		$this->update = "update $this->table";
	}

	function constructFilteringQuery($columns) {
		$filter = $this->selectAll."\n";
		if($columns == null){
			return $filter;
		}
		$filter .= $this->where.' ';
		$restofQuery = $this->stringPrep($columns);
		$filter .= $restofQuery;
		return $filter;
	}

	function constructCreateQuery($columns) {
		//starts the create query with the insert syntax of mysql
		$createQueryKeys = $this->insert;
	 	$createQueryValues = " values (";
	 	$numberOfColumns = count($columns)-1;
	 	$columnsCount = 0;
	 	foreach($columns as $fieldKey => $fieldValue) {
	 		$createQueryKeys .= "$fieldKey";
	 		$createQueryValues .= "'".$fieldValue."'";
	 		if ($columnsCount < $numberOfColumns) {
	 			$createQueryKeys .= ", ";
	 			$createQueryValues .= ", ";
	 			$columnsCount++;
	 		}
	 	}
	 	$createQueryKeys .= ')';
	 	$createQueryValues .= ')';
	 	$createQuery = $createQueryKeys.$createQueryValues;
	 	return $createQuery;
	}
	//query part where 'and' seperates them...
	private function stringPrep($columns) {
		$query;
		$length = count($columns)-1;
		//counter to know on what column number i am on
		$colNum = 0;
		foreach($columns as $name => $value) {
			$query .= $name.'='."'".$value."'";
			if($colNum < $length) {
				$query .= " and\n";
				$colNum++;
			}
		}
		return $query;
	}

	function constructDeleteQuery($columns) {
		$delete = $this->delete."\n".$this->where.' ';
		$restofQuery = $this->stringPrep($columns);
		$delete .= $restofQuery;
	 	return $delete;
	}
	
	function constructUpdateQuery($columns) {
		echo "\n";
		$update = $this->update."\n";
		$set = $this->set.' ';
		$where;
		$colLength = count($columns) - 1;
		$colCounter = 0;
		foreach($columns as $colKey => $colVal) {
			$set .= $colKey.'='."'".$colVal."'";
			if($colCounter < $colLength) {
				$set .= ',';
				$colCounter++;
			}
			$set .= "\n";
		}
		if(empty($_GET)){
			$where = '';
		} else {
			$where = $this->where.' ';
			$whereLength = count($_GET) - 1;
			$whereCounter = 0;
			foreach($_GET as $whereKey => $whereVal) {
				$where .= $whereKey.'='."'".$whereVal."'";
				if($whereCounter < $whereLength) {
					$where .= 'and';
					$whereCounter++;
				}
				$where .= "\n";
			}
		}
		$update .= $set.$where;
		return $update;
	}
}