<?php
require_once('../DB.php');
require_once('queries.php');

class Model {
	//*************************add feild names plz!!*************************//
	//table name in which is the same as DB table name...
	private $table;
	//query to select all rows from table
	private $queries;
	function __construct() {
		$this->table = $this->getClassName();
		$this->queries = new queries($this->table);
	}
	private function getClassName() {		
		return get_class($this);
	}
	//might use this later...
	private function getColumnNumber() {
		$className = $this->getClassName();
		$query = $this->selectAll."_id = '1'";
    	return getFieldCount($query);
	}
	// query with filtered data...
	function where($columns = null) {
		$query = $this->queries->constructFilteringQuery($columns);
		return dbQuery($query);
	}
	//create query function sends create query for execution...
	function createRow($columns) {
	 	$create = $this->queries->constructCreateQuery($columns);
	 	return dbQuery($create);
	}

	function update($columns) {
		$query = $this->queries->constructUpdateQuery($columns);
		return dbQuery($query);
		
	}
	//delete query function sens for execution...
	function deleteRows($columns) {
		$deleteQuery = $this->queries->constructDeleteQuery($columns);	
		$result = dbQuery($deleteQuery);
		return $result;
	}		
}