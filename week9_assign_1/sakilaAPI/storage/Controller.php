<?php

class Controller {
	//to become instance of Model to what ever this controller is inherited to
	private $model;
	//number of hit
	private $limit;


	function __construct() {
		//get all required files to this and child classes
		$includedFiles = get_included_files();
		//concatenating 'class' to the filename of included file which is the class name of that model...
		$modelname = pathinfo($includedFiles[2], PATHINFO_FILENAME);
		$this->model = new $modelname();
	}

	private function getAllModel($columns = null) {
		if($columns == null){
			$result = $this->model->where();
		} else {
			$result = $this->model->where($columns);
			if($result == false) {
				echo ', so change the feild name please.';
				return;
			}
			if($result->num_rows < 1) {
				echo 'none found!';
				return;
			}
		}
		$jsonRows = $this->prepareJSONResult($result);
		//return searched values as json object...
		echo json_encode($jsonRows, JSON_PRETTY_PRINT);
	}
	//encodes blob files to base_64...
	private function encode($value) {
		$encoded = base64_encode($value);
		return $encoded;
	}

	private function prepareJSONResult($result) {
		$jsonRows;
		while($row = $result->fetch_assoc()) {
			$jsonRow;
			foreach($row as $key => $value) {

				$jsonRow[$key] = $value;
			}
			//echo $jsonRow['picture'];
			if($jsonRow['picture'] != null) {
				$jsonRow['picture'] = $this->encode($jsonRow['picture']);
			}
			if(isset($jsonRow['location'])) {
				//if there is a location set, then it is a blob file and must be coded to base64 so it can then be encoded into a json file...
				$jsonRow['location'] = $this->encode($jsonRow['location']);
			}
			$jsonRows[] = $jsonRow;
		}
		return $jsonRows;
	}

	private function createRow($columns = null) {
		if($columns == null) {
			echo 'oops!, no data was received... from you... :/';
			return;
		}
		$result = $this->model->createRow($columns);
		if($result == false) {
			echo ', so change the feild name please.';
			return;
		}
		$this->getAllModel($columns);
	}

	private function updateRow() {
		echo "reached method update\n";
		var_dump($_GET);
		// if(count($_GET) > 1) {
		// 	echo 'only one feild to find what to update!!';
		// 	return;
		// }
		$putData = file_get_contents("php://input");
		$putData = explode('&', $putData);
		$setData;
		foreach($putData as $data) {
			$splitData = explode('=', $data);
			$setData[$splitData[0]] = $splitData[1];
		}
		if($this->model->update($setData)) {
			$this->getAllModel($setData);
		}
	}

	private function deleteRow() {
		if(empty($_GET)) {
			echo 'no data sent!!';
			return;
		}
		$result = $this->model->deleteRows($_GET);
	}

	function parse() {
			$request = $_SERVER['REQUEST_METHOD'];
			switch($request) {
				case 'GET':
					$this->getAllModel($_GET);
					break;
				case 'POST':
					$this->createRow($_POST);
					break;
				case 'PUT':
					$this->updateRow();
					break;
				case 'DELETE':
					$this->deleteRow();
					break;
			}
	}
}