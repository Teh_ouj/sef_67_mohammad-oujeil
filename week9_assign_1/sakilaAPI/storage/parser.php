<?php

class parser {
	private $controllerClass;

	function __construct($className) {
		$controllerClassName = $this->getControllerName();
		$this->controllerClass = new $className();
	}

	private function getControllerName() {		
		$includedFiles = get_included_files();
		$controllerClassName = pathinfo($includedFiles[1], PATHINFO_FILENAME);
		return $controllerClassName;
	}
}	
