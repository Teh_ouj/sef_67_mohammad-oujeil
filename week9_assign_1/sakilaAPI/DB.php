<?php

function dbConnect() {
    //static variable, to avoid connecting more than once 
    static $connection;
    // Try and connect to the database, if a connection has not been established yet
    if(!isset($connection)) {

         // Load configuration as an array.
        $config = parse_ini_file('../config.ini'); 
        $connection = new mysqli('localhost',$config['username'],$config['password'],$config['dbname']);
    }

    if($connection->connect_error) {
        return $connection->connect_error; 
    } else {
        //this should never be accessed!!
    }
    return $connection;
}

function dbQuery($query, $connection = null) {
    if(!isset($connection)){
        $connection = dbConnect();
    }
    $result = $connection->query($query);
    echo $connection->error;
    return $result;
}

function getFieldCount($query) {
    $connection = dbConnect();
    dbQuery($query, $connection);
    return $connection->field_count;
}