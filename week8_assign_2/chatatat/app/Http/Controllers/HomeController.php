<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Message;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::all();
        $channel = Channel::where('channel_id', 1)->first();
        if(count($channel) < 1) {
            return redirect('/home/create/general');
        }
        $channel = Channel::all();
        $users = User::all();
        return view('home')->with(['channels' => $channel, 'messages' => $messages, 'users' => $users]);
    }

    // public function show($id) {
    //     //$messages = Channel::all()->where('channel_id', $id)->messages();
    //     //var_dump($messages);
    // }

    public function createGeneral() {
        $channel = new Channel;
        $channel->name = 'General';
        $channel->owner_id = auth::user()->id;
        $user = User::find(1);
        $user->super=true;
        $channel->save();
        echo 'yo';
        return redirect('/home');
    }
}
