<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Channel;

class ApiController extends Controller
{
    //
    public function store() {
        $msg = request('msg');
        $channelID = request('channel_id');
        $username = request('username');
        $message = new Message;
        $message->username = $username;
        $message->channel_id = $channelID;
        $message->message = $msg;
        $message->save();
        echo 'done';
    }

    public function createChannel ($id) {
        $name = $_POST["channel"];
        $channel = new Channel;
        $channel->name = $name;
        $channel->owner_id = $id;
        $channel->save();
        echo $channel;
    }
}
