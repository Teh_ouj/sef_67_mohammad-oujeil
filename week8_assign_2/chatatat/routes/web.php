<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/home/create/general', 'HomeController@createGeneral');
Route::post('/home/create/{id}', 'ApiController@createChannel');
//Route::get('/home/channel/', 'HomeController@show');
Route::post('/home/store', 'ApiController@store');
