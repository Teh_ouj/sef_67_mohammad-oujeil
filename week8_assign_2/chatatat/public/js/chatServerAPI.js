var socket;

function createSocket(host) {

    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);

    throw new Error("No web socket support in browser!");
}

function init() {
    var host = "ws://localhost:12345/chat";
    try {
        socket = createSocket(host);
        log('WebSocket - status ' + socket.readyState);
        socket.onopen = function(msg) {
            socket.send(username)
            log("Welcome - status " + this.readyState);
        };
        socket.onmessage = function(msg) {
            log(msg.data);
        };  
        socket.onclose = function(msg) {
            log("Disconnected - status " + this.readyState);
        };
    }
    catch (ex) {
        log(ex);
    }
    document.getElementById("msg").focus();
}

function send() {
    var messageInput = document.getElementById('msg');
    var value = document.getElementById('msg').value;
    var msgBigParent = messageInput.parentElement.parentElement.parentElement;
    var channelId = msgBigParent.id;
    var msg = {
        username : username,
        user_id : id,
        channel_id : channelId,
        msg : value
    };
    console.log(msg);
    try {
        socket.send(JSON.stringify(msg));
    } catch (ex) {
        log(ex);
    }
}
function quit() {
    log("Goodbye!");
    socket.close();
    socket = null;
}

function log(msg) {
    document.getElementById("log").innerHTML += '<p id="message">' + msg + '</p>';
}

function onkey(event) {
    //console.log('dude');
    if (event.keyCode == 13) {
        //  console.log('send');
        
        send();
    }
}

function getMessageBox() {
    var id = this.getAttribute('name');
    var hiden = document.getElementsByClassName("Chatbox-message-veiw");
    for (var i = 0; i< hiden.length; i++) {
        hiden[i].style.display='none';
    }
    console.log(id + ' is clicking');
    var channel = document.getElementById(id);
    channel.style.display='block';
}
window.onload =function() {
    var message = document.getElementById("msg");
    message.addEventListener('keypress', onkey, false);
    var channelClick = document.getElementsByClassName("show-channel");
    console.log(channelClick.length);
    for(var i = 0; i< channelClick.length; i++) {
        var id = channelClick[i].getAttribute('name');
        console.log(id);
        channelClick[i].addEventListener('click', getMessageBox);
    }

    init();

};





