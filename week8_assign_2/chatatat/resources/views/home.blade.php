@extends('layouts.app')

@section('content')
<div class="container">
	<div class="chatbox-interface row">
		<div class="col-md-3 hold-all-info">
			<div class="panel panel-default row chatbox-sidebar">
				<div class="custom-search-input">
					<div class="input-group col-md-12 search-bar">
	                  	<input type="text" class="search-query form-control" placeholder="Conversation" />
	                  	<button class="btn btn-danger" type="button">
	                  		<img src="{{url('/image/magnifying-glass.png')}}"></img>
	                  	</button>
	               	</div>
				</div>
				<div class="dropdown-add_channel col-md-12 text">
					<a data-toggle="collapse" data-target="#demo">Add channel</a>
				 	<div id="demo" class="collapse text-left">
					    <ul>
					    	<li>
					    	<input type="text" id="channel-to-add">
					    	<button type="submit" id="add-channel">add</button>	
					    	</li>
					    </ul>
					</div>
				</div>	
				<div class="dropdown-channels col-md-12 text">
					<a data-toggle="collapse" data-target="#demo1">Channels</a>
				 	<div id="demo1" class="collapse text-left">
					    <ul>
					    	@foreach($channels as $channel)
					    	<li><a class="show-channel" name="{{$channel->channel_id}}">{{$channel->name}}</a></li>
					    	@endforeach
					    </ul>
					</div>
				</div>		
				<div class="dropdown-Private col-md-12 text">
					<a data-toggle="collapse" data-target="#demo2">Private messages</a>
			 		<div id="demo2" class="collapse">
						<div class="list-group">
						@foreach($users as $user)
							<a href="#" class="list-group-item">
								<div class="col-img">
									<img class="media-img" src="{{url('/image/avatar.jpg')}}">
								</div>
								<div class="col-info-personal-chat">
								    <h4 class="list-group-item-heading">{{$user->username}}</h4>
								    <p class="list-group-item-text">{{$user->bio}}</p>
								</div>
							</a>
						@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		@foreach($channels as $channel)
		<div class="Chatbox-message-veiw col-md-9" id="{{$channel->channel_id}}" hidden>
			<div class="panel panel-default row chatbox-message-view">
				<div class="panel-heading">Title</div>
				<div class="panel-body chatbox-message-view-body" id="log">
				@for($i = 0; $i < count($messages); $i++)
					@if($messages[$i]->channel_id == $channel->channel_id)
						<p>{{$messages[$i]->username}} said: {{$messages[$i]->message}} </p>
						<p class="left-block">SENT AT {{$messages[$i]->created_at}}</p>
					@endif
				@endfor
				</div>
				<div class="panel-footer-input">
					<input type="text" class="chat-text-input form-control" placeholder="Conversation" id="msg" />
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="footer">
	dude
</div>
<script type='text/javascript'>
	var username = '{{auth::user()->username}}';
	var id = '{{auth::user()->id}}';
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="{{url('js/chatServerAPI.js')}}"></script>
<script type="text/javascript" src="{{url('js/test.js')}}"></script>
@endsection
